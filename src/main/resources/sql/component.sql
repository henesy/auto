### 通知公告列表
#sql("base.notice.list")
	select * from t_com_notice
	
	where 1 = 1
	#if(keyword)
		and (title like #like(keyword) or src like #like(keyword) or issure like #like(keyword) or content like #like(keyword))
	#end
	#if(userId!=null)
		and not exists (select 1 from t_log_reading where t_log_reading.data_id=t_com_notice.id and t_log_reading.type='t_com_notice' and t_log_reading.user_id=#para(userId))
	#end	
	#if(type)
		and state = #para(type)
	#end	
	#if(state)
		and type = #para(state)
	#end
	#if(stime)
		and stime >= #para(stime)
	#end	
	#if(etime)
		and stime <= #para(etime)
	#end
	#order()
	
#end

### 知识库类型列表
#sql("doc.type.list")
	select * from t_com_doc_type
	
	where 1 = 1
	#if(keyword)
		and name like #like(keyword)
	#end
	#if(state)
		and state = #para(state)
	#end
	#if(null!=id)
		and parent_id = #para(id)
	#end	
	#order()
	
#end

### 知识库列表
#sql("doc.list")
	select t_com_doc.*,t_com_doc_type.name type_name,t_sys_user.name user_name 
	from t_com_doc left join t_com_doc_type on t_com_doc.type_id=t_com_doc_type.id
	left join t_sys_user on t_com_doc_type.luser=t_sys_user.id
	
	where 1 = 1
	#if(keyword)
		and (title like #like(keyword) or keyword like #like(keyword) or remark like #like(keyword))
	#end
	#if(state)
		and t_com_doc.state = #para(state)
	#end
	#if(type)
		and t_com_doc_type.path like #like(","+type+",")
	#end
		
	#order()
	
#end

### 系统反馈列表
#sql("feedback.list")
	select * from t_com_feedback
	
	where 1 = 1
	#if(keyword)
		and (title like #like(keyword) or content like #like(keyword) or reply_content like #like(keyword))
	#end
	#if(state)
		and state = #para(state)
	#end
	#if(type)
		and type = #para(type)
	#end	
	#if(stime)
		and ctime >= #para(stime)
	#end	
	#if(etime)
		and ctime <= #para(etime)
	#end
	#order()
	
#end

### 系统消息列表
#sql("msg.list")
	select t_com_msg.*,t_sys_user.name from_name,t_com_msg_box.read_time from t_com_msg
	left join t_com_msg_box on t_com_msg_box.msg_id=t_com_msg.id
	left join t_sys_user on t_com_msg.from_id=t_sys_user.id
	
	where t_com_msg_box.type = #para(type) and t_com_msg_box.user_id = #para(userId)
	#if(keyword)
		and (subject like #like(keyword) or content like #like(keyword))
	#end
	#if(stime)
		and ctime >= #para(stime)
	#end	
	#if(etime)
		and ctime <= #para(etime)
	#end
	#if(unread)
		and t_com_msg_box.read_time is null
	#end
	#order()
	
#end