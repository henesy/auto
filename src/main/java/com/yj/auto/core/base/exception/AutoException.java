package com.yj.auto.core.base.exception;

/**
 * 公共异常类，预留，便于后续处理
 */
public class AutoException extends Exception {

	public AutoException() {
		super();
	}

	public AutoException(String msg) {
		super(msg);
	}

	public AutoException(String msg, Throwable e) {
		super(msg, e);
	}
}
