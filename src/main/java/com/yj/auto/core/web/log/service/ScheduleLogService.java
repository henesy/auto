package com.yj.auto.core.web.log.service;

import com.jfinal.kit.StrKit;
import com.jfinal.log.Log;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.SqlPara;
import com.yj.auto.core.base.annotation.Service;
import com.yj.auto.core.base.model.QueryModel;
import com.yj.auto.core.jfinal.base.BaseService;
import com.yj.auto.core.web.log.model.ScheduleLog;

/**
 * TLogSchedule 管理 描述：
 */
@Service(name = "scheduleLogSrv")
public class ScheduleLogService extends BaseService<ScheduleLog> {

	private static final Log logger = Log.getLog(ScheduleLogService.class);

	public static final String SQL_LIST = "log.schedule.list";

	public static final ScheduleLog dao = new ScheduleLog().dao();

	@Override
	public ScheduleLog getDao() {
		return dao;
	}

	@Override
	public SqlPara getListSqlPara(QueryModel query) {
		if (StrKit.isBlank(query.getOrderby())) {
			query.setOrderby("stime desc");
		}
		return getSqlPara(SQL_LIST, query);
	}

	public boolean delete(String keyword, String stime, String etime) {
		QueryModel query = new QueryModel();
		query.setKeyword(keyword);
		query.setStime(stime);
		query.setEtime(etime);
		SqlPara sql = getSqlPara("log.schedule.clear", query);
		return Db.update(sql) > 0;
	}
}