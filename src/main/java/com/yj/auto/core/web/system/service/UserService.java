package com.yj.auto.core.web.system.service;

import java.util.List;

import com.jfinal.kit.StrKit;
import com.jfinal.log.Log;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import com.jfinal.plugin.activerecord.SqlPara;
import com.yj.auto.core.base.annotation.Service;
import com.yj.auto.core.base.model.QueryModel;
import com.yj.auto.core.jfinal.base.BaseService;
import com.yj.auto.core.web.system.model.User;
import com.yj.auto.core.web.system.model.UserRole;

/**
 * User 管理 描述：
 */
@Service(name = "userSrv")
public class UserService extends BaseService<User> {

	private static final Log logger = Log.getLog(UserService.class);

	public static final String SQL_LIST = "system.user.list";
	public static final String ATTA_USER_PHOTO = "user_photo";
	public static final User dao = new User().dao();
	public static final UserRole roleDao = new UserRole().dao();

	@Override
	public User getDao() {
		return dao;
	}

	@Override
	public SqlPara getListSqlPara(QueryModel query) {
		if (StrKit.isBlank(query.getOrderby())) {
			query.setOrderby("sort");
		}
		return getSqlPara(SQL_LIST, query);
	}

	// 查询机构下的所有用户（包括子机构）
	public List<User> findByOrg(Integer orgId, String state) {
		QueryModel query = new QueryModel();
		query.setOrgId(orgId);
		query.setState(state);
		query.setOrderby("sort");
		return find(query);
	}

	// 查询所有当前机构下的用户（不包括子机构）
	public List<User> findByDept(Integer deptId, String state) {
		QueryModel query = new QueryModel();
		query.put("deptId", deptId);
		query.setState(state);
		query.setOrderby("sort");
		return find(query);
	}

	// 根据登录账号查询
	public User getByCode(String code) {
		String sql = "select * from t_sys_user where code=?";
		return dao.findFirst(sql, code);
	}

	public List<UserRole> findAllRoles(Integer userId) {
		String sql = "select t_sys_role.*,t_sys_user_role.user_id from t_sys_role left join t_sys_user_role on t_sys_role.id=t_sys_user_role.role_id and t_sys_user_role.user_id=?";
		List<UserRole> list = roleDao.find(sql, userId);
		return list;
	}

	public boolean save(User model) {
		boolean success = super.save(model);
		if (success && null != model.getRoles()) {
			saveRole(model.getId(), model.getRoles());
		}
		return success;
	}

	public boolean update(User model) {
		boolean success = super.update(model);
		if (success && null != model.getRoles()) {
			deleteRole(model.getId());
			saveRole(model.getId(), model.getRoles());
		}
		return success;
	}

	public boolean delete(Integer... ids) {
		deleteRole(ids);
		return super.delete(ids, UserService.ATTA_USER_PHOTO);
	}

	public List<UserRole> getRoles(Integer id) {
		String sql = "select * from t_sys_user_role where user_id=?";
		List<UserRole> list = roleDao.find(sql, id);
		return list;
	}

	private void saveRole(Integer id, List<UserRole> roles) {
		if (null == roles)
			return;
		for (UserRole r : roles) {
			r.setUserId(id);
			r.save();
		}
	}

	private void deleteRole(Integer... id) {
		roleDao.deleteByColumn("user_id", id);
	}

}