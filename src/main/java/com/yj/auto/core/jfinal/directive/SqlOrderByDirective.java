package com.yj.auto.core.jfinal.directive;

import java.util.Map;

import com.jfinal.kit.StrKit;
import com.jfinal.template.Directive;
import com.jfinal.template.Env;
import com.jfinal.template.io.Writer;
import com.jfinal.template.stat.Scope;

public class SqlOrderByDirective extends Directive {

	@Override
	public void exec(Env env, Scope scope, Writer writer) {
		String key = exprList.length()<1 ? "orderby" : exprList.getExpr(0).toString();
		Map data = scope.getData();
		if (data != null && StrKit.notNull(data.get(key))) {
			String order = data.get(key).toString();
			if (StrKit.notBlank(order))
				this.write(writer, " order by " + order);
		}
	}
}
