package com.yj.auto.plugin.generator;

import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.generator.ColumnMeta;
import com.yj.auto.Constants;

public class AutoColumnMeta extends ColumnMeta {
	public int size;// 字段长度
	public int decimal;// 小数点
	public Boolean isUnique = false; // 是否唯一键，只支持单个字段
	public String prefix = null;// model名称

	public boolean isFloat() {
		if ("Float".equalsIgnoreCase(javaType) || "Double".equalsIgnoreCase(javaType) || "BigDecimal".equalsIgnoreCase(javaType)) {
			return true;
		}
		return false;
	}

	public boolean isInteger() {
		if ("Byte".equalsIgnoreCase(javaType) || "Short".equalsIgnoreCase(javaType) || "Integer".equalsIgnoreCase(javaType) || "Long".equalsIgnoreCase(javaType)) {
			return true;
		}
		return false;
	}

	public boolean isNumeric() {
		return isInteger() || isFloat();
	}

	public boolean isDate() {
		if ("Date".equalsIgnoreCase(javaType) || "Timestamp".equalsIgnoreCase(javaType) || "Time".equalsIgnoreCase(javaType)) {
			return true;
		}
		return false;
	}

	public boolean isString() {
		if ("String".equalsIgnoreCase(javaType)) {
			return true;
		}
		return false;
	}

	public boolean isPK() {
		return StrKit.notBlank(isPrimaryKey);
	}

	public boolean isRequired() {
		return "NO".equals(isNullable);
	}

	/**
	 * 生成字段的表单模板
	 * 
	 * @return
	 */
	public String toHtml() {
		if (StrKit.isBlank(prefix)) {
			prefix = "model.";
		} else if (!prefix.endsWith(".")) {
			prefix += ".";
		}

		StringBuffer valid = new StringBuffer("");

		if (isRequired()) {
			valid.append(" required");
		}
		if (isDate()) {
			valid.append(" datetimepicker=\"yyyy-mm-dd\"");
		} else if (isNumeric()) {
			if (decimal > 0) {
				valid.append(" data-bv-numeric");
			} else {
				valid.append(" data-bv-integer");
			}
		}
		if (size > 0) {
			valid.append(" maxlength=\"").append(size).append("\"");
		}
		boolean text = false;
		if (isString() && size > 1000) {
			text = true;
		}
		StringBuffer html = new StringBuffer();

		html.append(" id=\"").append(prefix).append(attrName).append("\"");
		html.append(" name=\"").append(prefix).append(attrName).append("\"");
		if (!text) {
			html.append(" value=\"");
			if (isDate()) {
				html.append("#date(").append(prefix).append(attrName).append(",\"yyyy-MM-dd\")");
			} else {
				html.append("#(").append(prefix).append(attrName).append(")");
			}
			html.append("\"");
		}
		html.append(" placeholder=\"").append(remarks).append("\"");
		html.append(" class=\"form-control input-md");
		if (isDate()) {
			html.append(" date");
		}
		html.append("\"");
		html.append(valid);

		StringBuffer result = new StringBuffer();
		if (text) {
			result.append("<textarea").append(html).append(">").append("#(").append(prefix).append(attrName).append(")</textarea>");
		} else {
			result.append("<input").append(html);
			if (isInteger()) {
				result.append(" type=\"number\"");
			}
			result.append("/>");
		}
		return result.toString();
	}

	/**
	 * 设置hibernate validator 注解
	 * 
	 * @return
	 */
	public String toValidator() {
		if (isPK())
			return "";
		StringBuffer result = new StringBuffer();
		if (isRequired()) {
			if (isString()) {
				result.append("@NotBlank");
				result.append(System.getProperty("line.separator"));
				result.append("	@Length(max = ").append(size).append(")");
			} else {
				result.append("@NotNull ");
			}
		} else if (isString()) {
			result.append("@Length(max = ").append(size).append(")");
		}
		return result.toString();
	}
}
