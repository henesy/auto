package com.yj.auto.plugin.table;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.jfinal.kit.StrKit;
import com.yj.auto.plugin.table.model.DataTables;

public class DataTableUtil {
	public static  DataTables getDataTableByJson(String json) {
		if (StrKit.isBlank(json))
			return null;
		DataTables dt = JSON.parseObject(json, new TypeReference<DataTables>() {
		});
		return dt;
	}
}
