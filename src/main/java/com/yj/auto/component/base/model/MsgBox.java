package com.yj.auto.component.base.model;

import com.yj.auto.core.base.annotation.*;
import com.yj.auto.component.base.model.bean.*;

/**
 * 系统消息文件夹
 */
@SuppressWarnings("serial")
@Table(name = MsgBox.TABLE_NAME, key = MsgBox.TABLE_PK, remark = MsgBox.TABLE_REMARK)
public class MsgBox extends MsgBoxEntity<MsgBox> {
	public static final String BOX_TYPE_INBOX = "01";// 收件箱
	public static final String BOX_TYPE_OUTBOX = "02";// 发件箱
	public static final String BOX_TYPE_DRAFT = "03";// 草稿箱
	public static final String BOX_TYPE_WASTE = "04";// 废件箱

}