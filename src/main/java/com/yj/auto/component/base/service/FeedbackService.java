package com.yj.auto.component.base.service;

import com.jfinal.kit.*;
import com.jfinal.log.Log;
import com.jfinal.plugin.activerecord.*;
import com.yj.auto.core.base.model.*;
import com.yj.auto.core.jfinal.base.*;
import com.yj.auto.core.base.annotation.*;
import com.yj.auto.Constants;
import com.yj.auto.component.base.model.*;

/**
 * Feedback 管理	
 * 描述：
 */
@Service(name = "feedbackSrv")
public class FeedbackService  extends BaseService<Feedback> {

	private static final Log logger = Log.getLog(FeedbackService.class);
	
	public static final String SQL_LIST = "component.feedback.list";
	
	public static final Feedback dao = new Feedback().dao();
	
	@Override
	public Feedback getDao() {
		return dao;
	}

	@Override
	public SqlPara getListSqlPara(QueryModel query) {
		if (StrKit.isBlank(query.getOrderby())) {
			query.setOrderby("ctime desc");
		}
		return getSqlPara(SQL_LIST, query);
	}	
   		
}