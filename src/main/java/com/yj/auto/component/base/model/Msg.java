package com.yj.auto.component.base.model;

import com.yj.auto.core.base.annotation.*;
import com.yj.auto.component.base.model.bean.*;
/**
 * 系统消息
 */
@SuppressWarnings("serial")
@Table(name = Msg.TABLE_NAME, key = Msg.TABLE_PK, remark = Msg.TABLE_REMARK)
public class Msg extends MsgEntity<Msg> {

}